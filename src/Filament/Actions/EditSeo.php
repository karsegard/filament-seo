<?php

namespace KDA\Filament\SEO\Filament\Actions;

use Closure;
use Filament\Facades\Filament;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Tables\Actions\Action;
use Illuminate\Http\RedirectResponse;
use KDA\Filament\SEO\Filament\Schema;
use KDA\SEO\Facades\SEO;
use Livewire\Redirector;

class EditSeo extends Action
{

    protected ?Closure $getLocaleUsing=null;

    public function locale(?Closure $callback):static{
        $this->getLocaleUsing = $callback;
        return $this;
    }

    public function getLocale():?string{
        return $this->evaluate($this->getLocaleUsing);
    }

    public static function getDefaultName(): ?string
    {
        return 'edit_seo';
    }
    protected function setUp(): void
    {
        parent::setUp();


        $this->action(function (array $data): void {
            $locale = $this->getLocale();
            $this->record->load('indexed');
            $seo = $this->record->indexed->where('locale',$locale)->first();
            $seo->fill($data);
            /*$seo>title = $data['title'];
            $seo>description = $data['description'];
            $seo>keywords = $data['keywords'];
            $seo>validated = $data['validated']??false;
            $seo>touched = true;*/
            $seo->save();
        });
        $this->icon('iconpark-seofolder-o');
        /*$this->mountUsing(fn (Forms\ComponentContainer $form, User $record) => $form->fill([
            'authorId' => $record->author->id,
        ]));*/
        $this->mountUsing(function ($form) {
            $locale = $this->getLocale();
            $seo = $this->record->indexed->where('locale',$locale)->first();
            if (!$seo) {
                $seo = SEO::createForModel($this->record,$locale);
                //$this->record->load('indexed');
            }
            //$rec = $this->record->indexed->attributesToArray();
            $form->fill($seo->attributesToArray());
        });
        $this->form(
            [
                Schema::getMetaSchema(),
                Schema::getOpenGraphSchema()->collapsed()
            ]
        );
    }
}
