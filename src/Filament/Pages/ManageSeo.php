<?php

namespace KDA\Filament\SEO\Filament\Pages;

use KDA\SEO\Settings\SeoSettings;
use BezhanSalleh\FilamentShield\Traits\HasPageShield;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\TextInput;
use Filament\Pages\SettingsPage;

class ManageSeo extends SettingsPage
{

    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static string $settings = SeoSettings::class;
   
     protected static function getNavigationLabel(): string
    {
        return __('filament-seo::pages.settings');
    }

    protected static function getNavigationGroup(): ?string
    {
        return __('filament.navigation.settings');
    }
    protected function getFormSchema(): array
    {
        return [
            TextInput::make('default_title')
                ->label('Default Title')
                ->required(),
            TagsInput::make('default_keywords')
                ->placeholder('new keyword')
                ->separator(', ')
                ->label('Default Keywords')
                ->required(),
            TextInput::make('default_description')
                ->label('Default Description')
                ->required(),
        ];
    }
}
