<?php

namespace KDA\Filament\SEO\Filament\Resources\SeoResource\Pages;

use KDA\Filament\SEO\Filament\Resources\SeoResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
class CreateSeo extends CreateRecord
{

    protected static string $resource = SeoResource::class;
    
}
