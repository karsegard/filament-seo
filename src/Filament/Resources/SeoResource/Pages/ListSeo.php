<?php

namespace KDA\Filament\SEO\Filament\Resources\SeoResource\Pages;

use KDA\Filament\SEO\Filament\Resources\SeoResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListSeo extends ListRecords
{
    protected static string $resource = SeoResource::class;

    protected function getActions(): array
    {
        return [
        ];
    }
}
