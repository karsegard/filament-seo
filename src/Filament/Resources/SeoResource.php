<?php

namespace KDA\Filament\SEO\Filament\Resources;

use Closure;
use KDA\Filament\SEO\Filament\Resources\SeoResource\Pages;
use KDA\Filament\SEO\Filament\Resources\SeoResource\RelationManagers;
use KDA\SEO\Models\SeoRecord;
use Filament\Forms;
use Filament\Forms\Components\Card;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\ViewField;
use Filament\Tables\Actions\Action;
use KDA\Filament\SEO\Filament\Schema;
use KDA\SEO\Facades\SEO;

class SeoResource extends Resource
{
    protected static ?string $model = SeoRecord::class;

    protected static ?string $navigationIcon = 'iconpark-seofolder-o';
    protected static ?string $navigationGroup = 'Settings';
    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->with('indexed');
    }

    protected static function getNavigationGroup(): ?string
    {
        return __('filament.navigation.seo');
    }
    protected static function getNavigationLabel(): string
    {
        return __('filament-seo::resources.seo');
    }

    

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make([
                    Placeholder::make('indexed')->label(__('filament-seo::panel.indexed_item'))->content(function ($record) {
                        return $record->load('indexed')->indexable;
                    }),
                    Grid::make(2)->schema([
                        Toggle::make('validated')->inline(false)->hint('Has been reviewed by user'),
                        Toggle::make('touched')->inline(false)->hint('Has been modified by user, prevents the system to automatically regenerate the record'),
                    ])

                ]),
                Grid::make(2)->schema([
                    Schema::getMetaSchema()->columnSpan(1),
                    // Forms\Components\Toggle::make('og')->inline(false)->reactive(),
                    Schema::getOpenGraphSchema()->columnSpan(1), //->hidden(fn (Closure $get) => $get('og') == false),
                ]),

                Forms\Components\Hidden::make('touched')->default(true)->dehydrateStateUsing(fn ($state) => true)
                /*Fieldset::make('Entity')
                    // ->relationship('billable')
                    ->schema([
                        Select::make('sluggable_type')
                        ->options(
                            Slug::all()->pluck('sluggable_type','sluggable_type')
                            )
                            ->default('App\Models\Customer')
                            ->reactive()
                            ->afterStateUpdated(function (callable $set) {
                                $set('sluggable_id', null);
                            }),
                            Select::make('sluggable_id')
                            ->searchable()
                            ->options(function (callable $get) {
                                $model = $get('sluggable_type');
                                if ($model) {
                                    return $model::all()->pluck('name', 'id');
                                }
                                return [];
                            })
                            ->required()
                            ->createOptionForm([
                                Forms\Components\TextInput::make('name')
                                ->required(),
                                
                                ])->createOptionUsing(function (array $data) {
                                    return \App\Models\Customer::create($data)->getKey();
                                }),
                                
                                ])*/
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->toggleable(),
                Tables\Columns\TextColumn::make('indexable')->toggleable(),
                Tables\Columns\BooleanColumn::make('validated')->toggleable(),
                Tables\Columns\BooleanColumn::make('touched')->toggleable()->label('Modifié'),
                //   Tables\Columns\TextColumn::make('slug')->searchable(),
                //Tables\Columns\TextColumn::make('url')->url(fn (SeoRecord $record): string => $record->url)->openUrlInNewTab(),
            ])
            ->filters([
                //
            ])
            ->headerActions([
             //   Action::make('generate_sitemap')->label(__('Generate Sitemap'))->action(fn()=>SEO::generateSitemap())
            ])
            ->actions([
                Tables\Actions\EditAction::make(),

            ])
            ->bulkActions([]);
    }

    public static function getRelations(): array
    {
        return [
            //

        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSeo::route('/'),
            'create' => Pages\CreateSeo::route('/create'),
            'edit' => Pages\EditSeo::route('/{record}/edit'),
        ];
    }
}
