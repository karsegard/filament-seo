<?php

namespace KDA\Filament\SEO\Filament;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\TagsInput;
use Filament\Forms\Components\Select;
use Closure;
class Schema {
    public static function getMetaSchema()
    {
        return Section::make('Meta tags')
            ->description('Standard meta tags')
            ->schema([
                TextInput::make('title')
                    ->maxLength(255),

                TextInput::make('description')
                    ->maxLength(255),
                TagsInput::make('keywords')
                    ->separator(', '),
            ]);
    }
    public static function getOpenGraphSchema()
    {
        return  Section::make('Open Graph')
        ->description('Advanced meta tags for social medias')
        
        ->schema([
            Repeater::make('opengraph')->label('')
                ->orderable(false)
                ->afterStateHydrated(function (Repeater $component, $state) {
                    $result = [];
                    $state = $state ?? [];
                    foreach ($state as $key => $value) {
                        $result[] = ["key" => $key, 'value' => $value];
                    }
                    $component->state($result);
                })
                ->mutateDehydratedStateUsing(function ($state) {
                    $result = [];
                    foreach ($state as $row) {
                        $result[$row['key']] = $row['value'];
                    }
                    return $result;
                })
                ->schema([
                    Select::make('key')->options([
                        'title' => 'title',
                        'url' => 'url',
                        'type' => 'type',
                        'description' => 'description'
                    ])->reactive(),
                    TextInput::make('value')->hidden(function (Closure $get) {
                        return $get('key') == 'type';
                    }),
                    Select::make('value')->hidden(function (Closure $get) {
                        return $get('key') !== 'type';
                    })->options([
                        'website' => 'website',
                        'article' => 'article'
                    ]),
                ])
        ]);
    }

}