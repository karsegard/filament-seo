<?php

namespace KDA\Filament\SEO;
use Filament\PluginServiceProvider;
use KDA\Filament\SEO\Filament\Resources\SeoResource;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
        \KDA\Filament\SEO\Filament\Pages\ManageSeo::class
    ];

    protected array $resources = [
   //     CustomResource::class,
        Filament\Resources\SeoResource::class
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-seo');
    }
}
