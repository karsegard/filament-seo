<?php
namespace KDA\Filament\SEO;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasTranslations;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasProviders;
    use \KDA\Laravel\Traits\HasDumps;
    use HasTranslations;

    protected $packageName = 'filament-seo';

    protected $dumps=[
        'seo_records',
        'seo_routes'
    ];
    /* This is mandatory to avoid problem with the package path */

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
        //register filament provider
    protected $additionnalProviders=[
        \KDA\Filament\SEO\FilamentServiceProvider::class
    ];
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){

    }
}
